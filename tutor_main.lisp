(in-package :snark-user)

(defun get-param (key param) (cdr (assoc key param)))


;; (defparameter *chinese-tea-ceremony-script*
;;   '((discuss chinese-tea-ceremony)
;;     (and (alias |\"Gongfu tea ceremony\"|)
;;      (when-where originate where Fujian |\"Chinese tea ceremony\"|))
;;     (if (response-contains "when")
;; 	(when-where originate when 1800ad |\"Chinese tea ceremony\"|)
;; 	(question when-where when |\"Chinese tea ceremony\"|)
;; 	())))
(defun flip-coin () (if (eq 0 (random 2)) t ()))
(defun randomly (list)
   (elt list (random (length list))))

(defun prompt-read (prompt)
  (format *query-io* "~a: " prompt)
  (force-output *query-io*)
  (read-line *query-io*))

(defun set-topic (x)  
  (define-compositional-nlg x  () "Chinese tea ceremony"))

(defparameter  *chinese-tea-ceremony*
  '((discuss chinese-tea-ceremony)
    (and (alias chinese-tea-ceremony gongfu-tea-ceremony)
	  (originate chinese-tea-ceremony Fujian))
    (originate-time  chinese-tea-ceremony 1800-A.D.)
    (means chinese-tea-ceremony making-tea-with-effort)
    (type-of-tea-used black-tea)
    (traditions-of two)
    (formal-tradition Hong-kong)
    (informal-tradition mainland)
    (representative hong-kong)))
(defvar *response* nil)
(defun chinese-tea-ceremony ()
    (define-compositional-nlg chinese-tea-ceremony  () "Chinese tea ceremony")
(defparameter *current-returning-phrase* "")

  (let ((sents *chinese-tea-ceremony*))
    (setf *response* ".")
    (nsay (pop sents))
    (define-compositional-nlg chinese-tea-ceremony  () "the ceremony")
    (dolist (curr sents)
     (nsay curr))
    (define-compositional-nlg chinese-tea-ceremony  () "Chinese tea ceremony")
    (challenge '(originate chinese-tea-ceremony Fujian))
    (challenge '(representative hong-kong)))
  (values))



(define-compositional-nlg originate (subj loc)
      subj " originated in " loc)
(define-compositional-nlg gongfu-tea-ceremony  () "Gongfu tea ceremony")

(define-compositional-nlg Fujian  () "Fujian")
(define-compositional-nlg alias  (x y)  x " is also known as the " y )
(define-compositional-nlg discuss (x) "let us talk about the " x )
(define-compositional-nlg means (x y) x " literally means " y)
(define-compositional-nlg type-of-tea-used (x) x  " is the type of tea used")
(define-compositional-nlg traditions-of (n) "there are " n " major traditions")
(define-compositional-nlg formal-tradition (f) "the "f " tradition is the formal tradition")
(define-compositional-nlg informal-tradition (i) "the " i " tradition is more informal" )
(define-compositional-nlg representative (r) "the " r  " tradition can be considered to be representative" )
(define-compositional-nlg originate-time (subj time) subj " originated in " time)


(defun originate-question-form (subj loc)
  (list  (concatenate 'string "where did the " (nlg subj nil) " originate?")
	 (nlg loc nil)))

(defun representative-question-form (r)
  (list  (concatenate 'string "Which tradition is considered to be the more representative tradition?")
	 (nlg r nil)))


(defun nlg? (form)
  (apply (symbol-function 
	  (read-from-string (concatenate 'string  (string-downcase (symbol-name (first form))) "-question-form")))
	 (rest form)))

(nlg?  '(originate chinese-tea-ceremony Fujian))

(nlg?  '(representative hong-kong))

(defun tutor-prompt (s)
  (format t "Tutor: ~a~%" s)
    (force-output t) )

(defparameter *fun-challengers* (list "Try this." "Can you answer this?" "Can you try this?"  "Answer this."))
(defun tutor-prompt-challenge(s)
  (let ((f  (randomly *fun-challengers*)))
    (format t "Tutor: ~a ~a~%" f s)

    (force-output t) 
    (say f)) (say s))

(defparameter *current-returning-phrase* "")
(defparameter *returning-phrases* 
  (list  "Now returning back: " "Picking from where I left: "))
(defun set-returning-phrase! () (setf *current-returning-phrase* (randomly *returning-phrases*  )))
(defun reset-returning-phrase! () (setf *current-returning-phrase* ""))

(defun nsay (s)
  (let ((nlged (concatenate 'string *current-returning-phrase* (nlg s))))
    (tutor-prompt nlged)
    (say  nlged)
    (setf *response* (prompt-read "You"))
    (cond 
      ((SBARQ? *response*)
       (progn (let ((ans (nlq *response*)))
		(tutor-prompt ans)
		(say (nlg ans)))
	      (set-returning-phrase!)))
      ((SQ? *response*)
       (progn (let ((ans (trick-yes-no)))
		(tutor-prompt ans)
		(say  ans))
	      (set-returning-phrase!)))
      (t (reset-returning-phrase!)))))

(defun challenge (form)
  (let* ((challenge (nlg? form))
	 (nlged (first challenge))
	 (answer (second challenge)))
    (tutor-prompt-challenge nlged)
    
    (setf *response* (princ-to-string (prompt-read "You")))
    (if (and (search answer *response*) (<= (length *response*) (* (length answer) 2 )))
	(progn (tutor-prompt "Yes! That is correct.") (say "Yes! That is correct."))
	(progn (tutor-prompt "Sorry! That is wrong. ") (say "Sorry! That is wrong.")))))


(defun trick-yes-no ()
  (if (flip-coin)
      (randomly (list "Probably, not." "I don't think so." "Rarely." "Maybe not. " "I don't know."))
      (randomly (list "Probably." "I think so." "Maybe." "Sometimes, but not always." "Maybe, you should find that out."))))
(defun say (s)
  (trivial-shell:shell-command (concatenate 'string "say " s ))
  )

(defparameter *narratives*
  '((:chinese-tea-ceremony )))

(defun get-narrative (topic)
  )
(defun tutor (topic)
  (chinese-tea-ceremony))

