
(ql:quickload "hunchentoot")




(defun print-bool (b) (if b "YES" "NO"))


(defun start-server ()
  (defparameter *ShouldIStart* nil)
  (defparameter *AreYouFinallyDoneDrinking* nil)
  (defparameter *AreYouFinallyDone* nil)
  (defparameter *NaoShouldGetCup?* nil)
  (hunchentoot:start (make-instance 'hunchentoot:easy-acceptor :port 4242)))



(hunchentoot:define-easy-handler (ShouldIStart :uri "/ShouldIStart") ()
  (setf (hunchentoot:content-type*) "text/plain")
  (format nil "~A" (print-bool *ShouldIStart*)))

(hunchentoot:define-easy-handler (AreYouFinallyDoneDrinking :uri "/AreYouFinallyDoneDrinking") ()
  (setf (hunchentoot:content-type*) "text/plain")
  (format nil "~A" (print-bool *AreYouFinallyDoneDrinking*)))

(hunchentoot:define-easy-handler (AreYouFinallyDone :uri "/AreYouFinallyDone") ()
  (setf (hunchentoot:content-type*) "text/plain")
  (format nil "~A" (print-bool *AreYouFinallyDone*)))

(hunchentoot:define-easy-handler (NaoGetCup :uri "/NaoGetCup") ()
  (setf (hunchentoot:content-type*) "text/plain")
  (setf *NaoShouldGetCup?* t)
  (format nil "OK"))


(hunchentoot:define-easy-handler (ShouldNaoReachForTheCup :uri "/ShouldNaoReachForTheCup") ()
  (setf (hunchentoot:content-type*) "text/plain")
  (format nil "~A" (print-bool *NaoShouldGetCup?*)))


(hunchentoot:define-easy-handler (FaceDetected :uri "/FaceDetected") ()
  (setf (hunchentoot:content-type*) "text/plain")
  (setf *ShouldIStart* t))

(hunchentoot:define-easy-handler (DemoStarted :uri "/DemoStarted") ()
  (setf (hunchentoot:content-type*) "text/plain")
  (setf *ShouldIStart* t)
(format nil "OK"))

(hunchentoot:define-easy-handler (DoneDrinking :uri "/DoneDrinking") ()
  (setf (hunchentoot:content-type*) "text/plain")
  (setf *AreYouFinallyDoneDrinking* t)
(format nil "OK"))

(hunchentoot:define-easy-handler (FinallyDone :uri "/FinallyDone") ()
  (setf (hunchentoot:content-type*) "text/plain")
  (setf *AreYouFinallyDone* t)
  (format nil "OK"))



