(in-package :snark-user)
(defparameter *add-associativity* 
  '(forall (?x ?y ?z)
    (= (+ (+ ?x ?y) ?z) (+ ?x (+ ?y ?z)))))
(defparameter *mult-associativity* 
  '(forall (?x ?y ?z)
    (= (* (* ?x ?y) ?z) (* ?x (* ?y ?z)))))
(defparameter *add-commutativity*
  '(forall (?x ?y)
    (= (+ ?x ?y) (+ ?y ?x))))
(defparameter *mult-commutativity*
  '(forall (?x ?y)
    (= (* ?x ?y) (* ?y ?x))))
(defparameter *add-distributivity*
  '(forall (?x ?y ?z)
    (= (* ?x (+ ?y  ?z)) (+ (* ?x ?y) (* ?x ?z)))))
(defparameter *mult-distributivity*
  '(forall (?x ?y ?z)
    (= (* (+ ?x ?y) ?z) (+ (* ?x ?z) (* ?y ?z)))))
(defparameter *add-identity*
  '(forall (?x)
    (and  (= (+ ?x 0) ?x ))))
(defparameter *mult-identity*
  '(forall (?x)
    (and  (= (* ?x 1) ?x ))))
(defparameter *add-inverse*
  '(forall (?x)
    (= (+ ?x (- ?x)) 0)))
(defparameter *mult-inverse*
  '(forall (?x)
    (implies (not (= ?x 0))
     (= (* ?x (inv ?x)) 1))))
(defparameter *total-order-1*
'(forall (?x ?y)
  (or (< ?x ?y) (< ?y ?x))))
(defparameter *total-order-2*
'(forall (?x ?y)
  (implies  (and (< ?x ?y) (< ?y ?x))
   (= ?x ?y))))
(defparameter *trans*
  '(forall (?x ?y ?z)
    (implies (and (< ?x ?y) (< ?y ?z))
      (< ?x ?z))))
(defparameter *add-ordering*
  '(forall (?x ?y ?z)
    (implies (< ?x ?y)
     (< (+ ?x ?z) (+ ?y ?z) ))))

(defparameter *mult-ordering*
  '(forall (?x ?y )
    (implies (and  (< 0 ?x) (< 0 ?y))
     (< 0 (* ?x ?y)))))

(defparameter *field-axioms* (list *add-associativity* *mult-associativity* 
				    *add-commutativity* *mult-commutativity*
				    *add-distributivity* *mult-distributivity*
				    *add-identity* *mult-identity*
				    *add-inverse* *mult-inverse*
				    *total-order-1*  *total-order-2* *trans* *add-ordering* *mult-ordering*))

(defun setup-field-theory ()
(initialize)
(use-resolution t)
;(use-hyperresolution t)
(use-paramodulation t)
(mapcar #'assert *field-axioms*))