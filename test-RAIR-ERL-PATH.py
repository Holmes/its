#!/usr/bin/env python
import urllib2, urllib
import sys
import time


query_args = {'problem':"(forall (?x) (implies (< 0 ?x) (< 0 (* ?x ?x))))"}

encoded_args = urllib.urlencode(query_args)

service_url = 'http://127.0.0.1:4242/provefieldtheorem?'

query_url = service_url+ encoded_args

start_time = time.time()
res= urllib2.urlopen(query_url).read()
elapsed_time = time.time() - start_time

print elapsed_time
print res
