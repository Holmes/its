(in-package :snark-user)
(defun replace-all (string part replacement &key (test #'char=))
"Returns a new string in which all the occurences of the part 
is replaced with replacement."
    (with-output-to-string (out)
      (loop with part-length = (length part)
            for old-pos = 0 then (+ pos part-length)
            for pos = (search part string
                              :start2 old-pos
                              :test test)
            do (write-string string out
                             :start old-pos
                             :end (or pos (length string)))
            when pos do (write-string replacement out)
            while pos)))
(defun run-parser () 
  (sb-ext:run-program 
   "java" 
   (list "-jar" "/Users/Naveen/work/dynamicQA/StanfordParserInterface/dist/StanfordParserInterface.jar")))

(defvar *parser-interface*)
(defvar *parser-interface-stream*)
(defvar *parser-interface-socket*)
(defun parser-interface-setup()
  (setq *parser-interface-socket* (usocket:socket-connect "127.0.0.1" 5000))
  (setq *parser-interface-stream* (usocket:socket-stream *parser-interface-socket*))) ()
(defun parse (s)
  (labels ((read-till-end (stream buffer)
	     (let* ((current-line (read-line *parser-interface-stream*))
		    (buffer (concatenate 'string buffer current-line)))
	       (if (not (equalp "" current-line))
		   (read-till-end stream buffer)
		   buffer)))) 	   
    (format *parser-interface-stream* s)
    (format *parser-interface-stream* "~%")
    (force-output *parser-interface-stream*) 
    (second (read-from-string 
	    (replace-all 
	     (replace-all (read-till-end *parser-interface-stream* "") "," "COMMA") 
	     "." "END")))))

(defun sparse (s)
  (labels ((read-till-end (stream buffer)
	     (let* ((current-line (read-line *parser-interface-stream*))
		    (buffer (concatenate 'string buffer current-line)))
	       (if (not (equalp "" current-line))
		   (read-till-end stream buffer)
		   buffer)))) 	   
    (format *parser-interface-stream* s)
    (format *parser-interface-stream* "~%")
    (force-output *parser-interface-stream*) 
     
    (replace-all 
     (replace-all (read-till-end *parser-interface-stream* "") "," "COMMA") 
     "." "END")))

(defun parseu (s)
  (let ((p (parse s)))
    (labels ((uniformize (tree)
	     (if (atom tree)
		 tree
		 `(tree ,(first tree) ,@(mapcar #'uniformize (rest tree))))))
      (uniformize p))))

(defun parser-interface-teardown ()
(usocket:socket-close *parser-interface-socket*))