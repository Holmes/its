
(in-package :snark-user)
;(load "~/work/dynamicQA/testdata.lisp")

(defun declare-signature ()
  (declare-sort 'tickersym)
  (declare-sort 'date)
  (declare-relation 'raw 6 :sort '(tickersym date number number number number number))
  (declare-function 'date 3 :sort '(date number number number))
  (declare-constant 'a :sort 'tickersym)
  (declare-constant 'b :sort 'tickersym)
  (declare-constant 'tesla :sort 'tickersym))

(defun snark-deverbose ()
  (snark:print-options-when-starting  nil)
  (snark:print-agenda-when-finished nil)
  (snark:print-clocks-when-finished nil)
  (snark:print-final-rows nil)
  (snark:print-symbol-table-warnings nil)
  (snark:print-summary-when-finished nil)
  (snark:print-row-answers nil)
  (snark:print-row-goals nil) 
  (snark:print-rows-when-derived nil)
  (snark:print-row-reasons nil)
  (snark:print-row-partitions nil)
  (snark:print-rows-prettily nil)
  (snark:print-rows :min 0 :max 0))

(defun setup-snark (&optional  (verbose nil))
  (snark:initialize :verbose  verbose)
  (if (not verbose) (snark-deverbose))
  ;;(snark:run-time-limit  5)
  (snark:assert-supported t)
  (snark:assume-supported t)
  (snark:prove-supported t)
  (snark:use-resolution t)
  (snark:use-paramodulation t)
  (snark:allow-skolem-symbols-in-answers nil)
  (snark::declare-code-for-numbers)
  (declare-signature)
  (declare-list-functions))

(defun declare-list-functions ()
  (snark::declare-code-for-lists)
  (declare-function '$$cons 2 :new-name 'cons)
  (declare-function '$$list :any :new-name 'list)
  (declare-function '$$list* :any :new-name 'list*)
  (declare-function '$$first 1  :sort '(t list):new-name 'first)
  (declare-function '$$rest 1 :sort '(list list) :new-name 'rest)

  (declare-relation '$$member 2 :new-name 'member)
  (declare-relation '$$subsetp 2 :new-name 'subsetp)

  (declare-function '$$union 2 :new-name 'union)
  (declare-function '$$set-difference 2 :new-name 'set-difference)

  (assert '(iff (subsetp ?x ?y) (implies (member ?p ?x) (member ?p ?y))))
  (assert '(member ?x (cons ?x ?y)))
  (assert '(implied-by 
	    (member ?x (cons ?z ?y))
	    (member ?x ?y)))

  (assert '(iff (append ?x ?y ?z) (= ?z (union ?x ?y))))

  ;; (assert '(iff (= ?z (set-difference ?x ?y))
  ;; 	    (forall ?p (iff (member ?p ?z)
  ;; 			(and (member ?p ?x) (not (member ?p ?y)))))))
  (assert '(= nil (first nil)))
  (assert '(= nil (rest nil)))
  (assert '(= ?x (first (cons ?x ?y))))
  (assert '(= ?y (rest (cons ?x ?y))))
  (assert '(reverse nil nil))
  (assert '(implied-by
	    (reverse (cons ?x ?l) ?l1)
	    (and
	     (reverse ?l ?l2)
	     (append ?l2 (cons ?x nil) ?l1))))
  (assert '(append nil ?l ?l))
  (assert '(implied-by
	    (append (cons ?x ?l1) ?l2 (cons ?x ?l3))
	    (append ?l1 ?l2 ?l3))))

(defun set-difference-rewriter (term subst)
  (dereference term subst
		:if-constant :none
		:if-variable :none
		:if-compound 
		
		(if (and (listp (first (args term))) (listp (second (args term))))
 		 (set-difference (first (args term)) (second (args term)))
		 :none)))

(defun declare-planning-signature ()
  (declare-sort 'fluent)
  (declare-sort 'action)
  (declare-relation 'allows 2 :sort '(list t))
  (declare-relation 'satisfies 3 :sort '(list list list))
  (declare-function 'effect 2 :sort '(list list list)))

(defparameter *planning-axioms*
  (list  
   ; Final stage
   '(forall ((?p list) (?s list))
     (satisfies ?s ?p ?s))
   ;Breaking down a plan
   '(forall ((?p list) (?s1 list) (?s2 list))
     (iff (satisfies ?s1 ?p ?s2)
      (and (allows ?s1 (first ?p)) (satisfies (effect ?s1 ?p)  (rest  ?p) ?s2))))
   ; A state allows an action
   '(forall ((?s list) (?a action))
   	   (iff (allows ?s ?a)
   		(subsetp (preconds ?a) ?s)))
   ; Effect of a plan on a state
   '(forall ((?p list) (?s list))
     (= (effect ?s ?p)
      (union (additions ?p) (set-difference ?s (subtractions ?p)))))
   ))


(defun planning-tests ()
  (labels ((declare-constants (type prefix)
	    (mapcar (lambda (x) (declare-constant x :sort type))
		    (let ((names nil))
		      (dotimes (n 10 names)
			(push (read-from-string (concatenate 'string prefix (princ-to-string (1+ n)))) 
			      names))))))
    (setup-snark)
    (declare-planning-signature)
    (declare-constants 'action  "a")
    (declare-constants 'fluent "f")
    (mapcar #'snark::assert *planning-axioms*)
    (mapcar #'snark::assert (list  '(allows (list f1) a1)
				   '(= (additions a1) ())
				   '(= (subtractions a1) ())))
    (prove '(satisfies (list f1) ?action1 (list f1)))))


(defun query (q &optional (verbose t))
  (setup-snark verbose)
  (mapcar #'snark::assert *simple-kb*)
  (second (snark:answer (prove q :answer '(ans _?X)))))


(defun nlq (q)
  (query 
   (understand-parse 
	  (parse q))
   nil))

(defun nao-query (q)
  (let ((ans (princ-to-string (nlq q))))
  (trivial-shell:shell-command 
   (concatenate 'string 
		"/Users/Naveen/work/dynamicQA/nao_tts.py"  
		" 10.0.2.6 "  " "
		ans))
  ans))
(defun get-ans (term) (second term))
(defun postprocess (par-sem)
  (cond ((null par-sem) nil)
	((atom par-sem) par-sem)
	(t  (if (equalp 'var (first par-sem))
		(mapcar #'postprocess (rest par-sem))
		(mapcar #'postprocess par-sem)))))
(defun understand-parse (parse &optional (verbose nil))
  (snark:initialize :verbose  verbose)
  (if (not verbose) (snark-deverbose))
  (snark:assert-supported t)
  (snark:assume-supported t)
  (snark:prove-supported t)
  (snark:use-factoring t)
  (snark:use-equality-factoring t)
  (snark:rewrite-answers t)
  (snark:use-resolution t)
  (snark:use-hyperresolution t)
  (snark:use-paramodulation t)
  (snark:allow-skolem-symbols-in-answers nil)
  (mapcar #'snark::assert *understanding-axioms*)
  (snark::assert `(uttered-parse ,parse))
  (postprocess
   (get-ans 
    (snark:answer 
     (snark:prove `(meaning ,parse ?meaning) :answer '(ans ?meaning))))))

