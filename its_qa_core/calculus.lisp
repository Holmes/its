(in-package :snark-user)


(defparameter  *simple-open*
  `(snark::forall 
    (?u ?sym ?day ?month ?year)
    (snark::implies
     (snark::and (uttered-parse ?u)
		 (= ?u  (sbarq (whnp (wp what))
			       (sq (vbd be)
				   (np (np (nnp ?sym) (pos 's))
				       (adjp (jj open) (pp (in on) (np (cd ?day) (cd ?month)))) (nn ?year))))))
     (meaning ?u 
	      (exists (var (var _?X number) (var _?a number) (var _?b number) (var _?c number) (var _?d number))
		      (raw ?sym (date ?day ?month ?year) _?X _?a _?b _?c _?d)))))
  "E.g What was tesla's open on 1 1 2013")


(defparameter  *simple-open*
  `(snark::forall 
    (?u ?sym ?day ?month ?year)
    (snark::implies
     (snark::and (uttered-parse ?u)
		 (= ?u  (sbarq (whnp (wp what))
			       (sq (vbd be)
				   (np (np (nnp ?sym) (pos 's))
				       (adjp (jj open) (pp (in on) (np (cd ?day) (cd ?month)))) (nn ?year))))))
     (meaning ?u 
	      (exists (var (var _?X number) (var _?a number) (var _?b number) (var _?c number) (var _?d number))
		      (raw ?sym (date ?day ?month ?year) _?X _?a _?b _?c _?d)))))
  "E.g What was tesla's open on 1 1 2013")

(defparameter  *simple-close*
  `(snark::forall 
    (?u ?sym ?day ?month ?year)
    (snark::implies
     (snark::and (uttered-parse ?u)
		 (= ?u  (sbarq (whnp (wp what))
			       (sq (vbd be)
				   (np (np (nnp ?sym) (pos 's))
				       (adjp (jj close) (pp (in on) (np (cd ?day) (cd ?month)))) (nn ?year))))))
     (meaning ?u 
	      (exists (var (var _?X number) (var _?a number) (var _?b number) (var _?c number) (var _?d number))
		      (raw ?sym (date ?day ?month ?year)  _?a _?b _?c _?X _?d)))))
  "E.g What was tesla's close on 1 1 2013")

(defparameter  *simple-high*
  `(snark::forall 
    (?u ?sym ?day ?month ?year)
    (snark::implies
     (snark::and (uttered-parse ?u)
		 (= ?u  (sbarq (whnp (wp what))
			       (sq (vbd be)
				   (np (np (nnp ?sym) (pos 's))
				       (adjp (jj high) (pp (in on) (np (cd ?day) (cd ?month)))) (nn ?year))))))
     (meaning ?u 
	      (exists (var (var _?X number) (var _?a number) (var _?b number) (var _?c number) (var _?d number))
		      (raw ?sym (date ?day ?month ?year)  _?a _?X _?b _?c _?d)))))
  "E.g What was tesla's high on 1 1 2013")

(defparameter  *simple-low*
  `(snark::forall 
    (?u ?sym ?day ?month ?year)
    (snark::implies
     (snark::and (uttered-parse ?u)
		 (= ?u  (sbarq (whnp (wp what))
			       (sq (vbd be)
				   (np (np (nnp ?sym) (pos 's))
				       (adjp (jj low) (pp (in on) (np (cd ?day) (cd ?month)))) (nn ?year))))))
     (meaning ?u 
	      (exists (var (var _?X number) (var _?a number) (var _?b number) (var _?c number) (var _?d number))
		      (raw ?sym (date ?day ?month ?year)  _?a _?b _?X _?c _?d)))))
  "E.g What was tesla's low on 1 1 2013")


(defparameter  *simple-volume*
  `(snark::forall 
    (?u ?sym ?day ?month ?year)
    (snark::implies
     (snark::and (uttered-parse ?u)
		 (= ?u  (sbarq (whnp (wp what))
			       (sq (vbd be)
				   (np (np (nnp ?sym) (pos 's))
				       (adjp (jj volume) (pp (in on) (np (cd ?day) (cd ?month)))) (nn ?year))))))
     (meaning ?u 
	      (exists (var (var _?X number) (var _?a number) (var _?b number) (var _?c number) (var _?d number))
		      (raw ?sym (date ?day ?month ?year)  _?a _?b _?c _?d _?X)))))
  "E.g What was tesla's volume on 1 1 2013")

;;;;;; 
(defparameter *who-verb-x*
  `(snark::forall
    (?action ?thing)
    (snark::implies
     (snark::and (uttered-parse ?u)
		 (= ?u (sbarq (whnp (wp who)) (sq (vp (vbd ?action) (np (nn ?thing)))))))
     (meaning ?u (exists _?X (invented _?X ?thing))))))

(defparameter *when/where-did-thing-verb*
  `(snark::forall
    (?verb ?thing ?type)
    (snark::implies
     (snark::and (uttered-parse ?u)
		 (= ?u (sbarq (whadvp (wrb ?type)) (sq (vbd do) (np (nn ?thing)) (vp (vb ?verb))))))
     (meaning ?u (exists _?X (When-where ?verb ?type _?X ?thing))))))

(defparameter *which-x-most-y*
  `(snark::forall 
    (?thing ?verb ?obj)
    (snark::implies
     (snark::and (uttered-parse ?u)
		 (= ?u
		    (SBARQ (WHNP (WDT WHICH) (NN ?thing)) (SQ (VP (VBZ ?verb) (NP (DT THE) (JJS MOST) (NN ?obj)))))))
     (meaning ?u (exists _?X (leads-in _?X ?verb ?obj))))))

(defparameter *which-activity-x*
  `(snark::forall
    (?thing ?verb ?obj)
    (snark::implies
     (snark::and (uttered-parse ?u)
		 (= ?u  (sbarq (whnp (wdt which) (nn ?thing)) (sq (vp (vbz ?verb) (np (nn ?obj)))))))
     (meaning ?u (exists _?X (activity  ?verb _?X ?obj))))))

(defparameter *simple-kb*
  (list 
    '(when-where originate where china tea)
   ;; '(when-where originate where ethiopia coffee)
   ;; '(when-where originate where georgia wine)
   ;; '(when-where originate when 10-b.c tea)
   ;; '(when-where originate when 1500ad coffee)
   ;; '(when-where originate when 6000bc wine)
   ;; '(activity produce |\"camellia sinensis\"| tea)
   ;; '(activity produce |\"coffea arabica\"| coffee)
   ;; '(activity produce |\"grape\"| wine)
   ;; '(leads-in china produce tea)
   ;; '(leads-in brazil produce coffee)
   ;; '(leads-in brazil produce wine))
  ))



(defparameter *understanding-axioms* 
  (list *who-verb-x* *when/where-did-thing-verb* *which-activity-x* *which-x-most-y*))



(defparameter *simple*
  '(snark::forall
    (SBARQ (WHNP (WP WHAT))
     (SQ (VBD BE)
      (NP (NP (NNP TESLA) (POS 'S))
	  (ADJP (JJ OPEN) (PP (IN ON) (NP (CD 1) (CD 1)))) (NN 2013))))))



(defun SBARQ? (q)
  (equalp  (first (parse q))
	   'SBARQ))

(defun SQ? (q)
  (equalp  (first (parse q))
	   'SQ))
;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; axioms for processing trees
(defparameter *tree-has-leaf*
  (list 
   '(tree-has-leaf (tree ?tag ?token) (leaf ?tag ?token)) 

   '(implied-by (tree-has-leaf (tree ?ttag ?sub) (leaf ?tag ?token))
     (tree-has-leaf ?sub (leaf ?tag ?token)))

   '(implied-by (tree-has-leaf (tree ?ttag ?sub1 ?sub2) (leaf ?tag ?token))
     (or 
      (tree-has-leaf ?sub1 (leaf ?tag ?token))
      (tree-has-leaf ?sub2 (leaf ?tag ?token))))

   '(implied-by (tree-has-leaf (tree ?ttag ?sub1 ?sub2 ?sub3) (leaf ?tag ?token))
     (or 
      (tree-has-leaf ?sub1 (leaf ?tag ?token))
      (tree-has-leaf ?sub2 (leaf ?tag ?token))
      (tree-has-leaf ?sub3 (leaf ?tag ?token))))

   '(implied-by (tree-has-leaf (tree ?ttag ?sub1 ?sub2 ?sub3 ?sub4) (leaf ?tag ?token))
     (or 
      (tree-has-leaf ?sub1 (leaf ?tag ?token))
      (tree-has-leaf ?sub2 (leaf ?tag ?token))
      (tree-has-leaf ?sub3 (leaf ?tag ?token))
      (tree-has-leaf ?sub4 (leaf ?tag ?token))))))

