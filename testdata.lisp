(in-package :snark-user)
(defparameter *test-data*
  '((tesla (date 1 1 2013) 21 20 30 25 400)
    (tesla (date 2 1 2013) 22 21 31 26 500)
    (tesla (date 3 1 2013) 24 23 32 23 200)
    (tesla (date 4 1 2013) 24 23 19 21 100)
    (GE (date 1 1 2013) 21 20 30 25 900)
    (GE (date 2 1 2013) 21 20 30 25 400)
    (GE (date 3 1 2013) 21 20 30 25 300)
    (GE (date 4 1 2013) 21 20 30 25 400)))

(defun convert-raw-data-into-clauses (raw-data varlist)
   (mapcar (lambda (row)
	      (destructuring-bind (tickersym date open high low close volume)  row
		(destructuring-bind (tx dx ox hx lx cx vx) varlist
		    `(and 
		      (= ,tx ,tickersym)
		      (= ,dx ,date)
		      (= ,ox ,open)
		      (= ,hx ,high)
		      (= ,lx ,low)
		      (= ,cx ,close)
		      (= ,vx ,volume)))))
	   raw-data))


(defparameter *test-kb*
  (list 
   `(forall ((?tick tickersym) (?date date) (?open number) (?high number) (?low number) (?close number) (?volume number))
     (implies (raw ?tick ?date ?open ?high ?low ?close ?volume)
      (or ,@(convert-raw-data-into-clauses *test-data* '( ?tick ?date ?high ?open ?low ?close ?volume)))))
   (cons 'and (mapcar #'(lambda (row) (cons 'raw row)) *test-data*))))

(defparameter *chinese-tea-culture*
  (list 
   '(invented |\"Emperor Shen nung\"| tea)
   '(when-where originate where china tea)
   '(when-where originate when 10bc tea)))